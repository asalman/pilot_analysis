Instructions for LXPLUS:

Clone and build this repo:

git clone https://gitlab.cern.ch/jwspence/pilot_analysis.git

cd pilot_analysis/Fedra/

source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.36/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh"

./install.sh

cd ..

Clone and build the fnumin simulation:

git clone https://gitlab.cern.ch/jwspence/fnumin.git -b pilotdetectortilted

source fnumin/setupLCG.sh LCG_95

mkdir build

cd build

cmake -DCMAKE_INSTALL_PREFIX=../run ../fnumin;make -j8;make install -j8

Execute the simulation:

cd ../run

bin/FASERnu -m ../fnumin/run1.mac -t 50

Copy simulation output to pilot_analysis directory:

cd ../pilot_analysis

cp ../run/FASERnu1.root .

Compile the digitization and tracking scripts:

./compiledigitize.sh

./compile_tracking.sh

Digitize the G4sim ROOT output file to FEDRA format:

./digitize

Execute the tracking script with

./tracking lnk.def

You need lnk.def, lnk.lst, default.par, and data/ containing the input CP files from the previous step or else tracking will not run.
